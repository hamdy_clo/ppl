
    <section class="content-header">
      <h1>
        Data Peminjaman
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
         
        </div> 
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Stok Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                  
               <tr>
                    <th>No</th>
                    <th>KODE BARANG</th>
                    <th>NAMA BARANG</th>
                    <th>JUMLAH</th>
                    <th>HARGA</th>
                    <th>TGL KLR</th>
                    <th>FOTO</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; 
                  foreach ($data_alat as $da){ ?>
                    <tr>
                     <td><?php echo $no++; ?></td>
                      <td><?php echo $da->kode_alat ?></td>
                      <td><?php echo $da->nama_alat ?></td>
                      <td><?php echo $da->jumlah ?></td>
                      <td><?php echo $da->harga ?></td>
                      <td><?php echo $da->tgl_keluar ?></td>
                      <?php if($da->foto==''){ ?>
                        <td><img class="responsive" src="<?php echo base_url(); ?>assets/gambar/no_pict.jpg" width="200px"></td>
                      <td>
                    </tr>
                  <?php } ?>

        <div class="modal modal-info fade" id="update<?php echo $da->kode_alat ?>">
          <div class="modal-dialog">
            <form action="<?php echo site_url('data_alat/update_alat') ?>" method="post" enctype="multipart/form-data" accept-chartset="utf-8">
                <input type="hidden" name="kode_alat" value="<?php echo $da->kode_alat ?>">
                <input type="hidden" name="foto_lama" value="<?php echo $da->foto ?>">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Data</h4>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                   <label>KODE BARANG</label>
                    <input type="text" name="nama_alat" class="form-control" required="" value="<?php echo $da->kode_alat; ?>">
                    <label>NAMA BARANG</label>
                    <input type="text" name="nama_alat" class="form-control" required="" value="<?php echo $da->nama_alat ?>">
                    <label>JUMLAH</label>
                    <input type="text" name="jumlah" class="form-control" required="" value="<?php echo $da->kode_alat; ?>">
                    <label>HARGA</label>
                    <input type="text" name="harga" class="form-control" required="" value="<?php echo $da->nama_alat ?>">
                    <label>TGL KELUAR</label>
                    <input type="text" name="tgl_keluar" class="form-control" value="<?php echo $da->jumlah ?>">
                    <label>FOTO</label>
                    <input type="file" name="foto" class="form-control" value="<?php echo $da->foto ?>">
                  </div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Update</button>
              </div>
            </div>
            </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal modal-danger fade" id="hapus<?php echo $da->kode_alat ?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data</h4>
              </div>
              <form action="<?php echo site_url('data_alat/hapus_alat') ?>" method="post">
                <input type="hidden" name="kode_alat" value="<?php echo $da->kode_alat ?>">
              <div class="modal-body">
                <p>Anda Yakin Akan Menghapus Alat ini?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Hapus</button>
              </div>
            </div>
            </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


                  <?php }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </section>
   
   <div class="modal modal-primary fade" id="tambah">
      <div class="modal-dialog">
        <form action="<?php echo site_url('data_peminjaman/tambah_peminjam') ?>" method="post" enctype="multipart/form-data" accept-chartset="utf-8">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tambah Peminjam</h4>
          </div>
          
          <div class="modal-body">
              <div class="form-group">
                <label>Nama Penerima</label>
                <input type="text" name="nama_penerima" class="form-control" required="">
                <label>Nama Penanggung Jawab</label>
                <input type="text" name="nama_penanggung_jawab" class="form-control" required="">
                <label>Lokasi</label>
                <input type="text" name="lokasi" class="form-control" required="">
              </div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Tambah Peminjam</button>
          </div>
        </div>
        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->