
    <section class="content-header">
      <h1>
        Data Peminjaman
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah">
            <i class="fa fa-plus"></i> Tambah Peminjam
          </button>
        </div> 
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Peminjaman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Nama Penerima</th>
                    <th>Nama Penanggung Jawab</th>
                    <th>Lokasi</th>
                    <th>Daftar Alat</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1;
                  foreach ($data_peminjaman as $dp){ ?>
                    <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $dp->tanggal_pinjam ?></td>
                    <td><?php echo $dp->nama_penerima ?></td>
                    <td><?php echo $dp->nama_penanggung_jawab ?></td>
                    <td><?php echo $dp->lokasi ?></td>
                    <td>
                      <a href="<?php echo site_url('admin/tambah_peminjaman/'.$dp->id_peminjaman.''); ?>"><i class="btn btn-warning fa fa-plus"> Tambah Alat</i></a>
                    </td>
                    <td><i class="btn btn-success fa fa-check fa fa-hourglass-half"> Proses</i></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </section>
   
   <div class="modal modal-primary fade" id="tambah">
      <div class="modal-dialog">
        <form action="<?php echo site_url('data_peminjaman/tambah_peminjam') ?>" method="post" enctype="multipart/form-data" accept-chartset="utf-8">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tambah Peminjam</h4>
          </div>
          
          <div class="modal-body">
              <div class="form-group">
                <label>Nama Penerima</label>
                <input type="text" name="nama_penerima" class="form-control" required="">
                <label>Nama Penanggung Jawab</label>
                <input type="text" name="nama_penanggung_jawab" class="form-control" required="">
                <label>Lokasi</label>
                <input type="text" name="lokasi" class="form-control" required="">
              </div> 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success">Tambah Peminjam</button>
          </div>
        </div>
        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->