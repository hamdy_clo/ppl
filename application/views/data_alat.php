
    <section class="content-header">
      <h1>
        BARANG MASUK
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <?php if(isset($_GET['kode_alat_ada'])){ ?>
          <div class="box-body">
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
                Kode Alat Yang Anda Masukkan Sudah Ada. Mohon Periksa Kembali Isian Anda.
              </div>
          </div>
        <?php }elseif (isset($_GET['tambah_alat'])) { ?>
          <div class="box-body">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Berhasil!</h4>
                Data Alat Berhasil Ditambahkan.
              </div>
            </div>
        <?php }elseif (isset($_GET['update_alat'])) { ?>
          <div class="box-body">
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Berhasil!</h4>
                Data Berhasil Di Update.
              </div>
            </div>
        <?php }elseif (isset($_GET['hapus_alat'])) { ?>
        <div class="box-body">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Terhapus!</h4>
                Data Berhasil di Hapus.
              </div>
          </div>
        <?php } ?>

        <div class="col-xs-12">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#tambah">
                <i class="fa fa-plus"></i> INPUT BARANG MASUK
              </button>
            </div>  
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">LIST BARANG MASUK</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>KODE BARANG</th>
                    <th>NAMA BARANG</th>
                    <th>JUMLAH</th>
                    <th>HARGA</th>
                    <th>TGL MSK</th>
                    <th>FOTO</th>
                    <th>AKSI</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; 
                  foreach ($data_alat as $da){ ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $da->kode_alat ?></td>
                      <td><?php echo $da->nama_alat ?></td>
                      <td><?php echo $da->jumlah ?></td>
                      <td><?php echo $da->harga ?></td>
                      <td><?php echo $da->tgl_msk ?></td>
                      <?php if($da->foto==''){ ?>
                        <td><img class="responsive" src="<?php echo base_url(); ?>assets/gambar/no_pict.jpg" width="100px"></td>
                      <td>
                      <?php }else{ ?>
                        <td><img class="responsive" src="<?php echo base_url(); ?>assets/gambar/<?php echo $da->foto ?>" width="80"></td>
                      <td>
                      <?php } ?>
                        <button class="btn btn-info" data-toggle="modal" data-target="#update<?php echo $da->kode_alat ?>">Update</button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#hapus<?php echo $da->kode_alat ?> ">Hapus</button>
                      </td>
                    </tr>

        <div class="modal modal-info fade" id="update<?php echo $da->kode_alat ?>">
          <div class="modal-dialog">
            <form action="<?php echo site_url('data_alat/update_alat') ?>" method="post" enctype="multipart/form-data" accept-chartset="utf-8">
                <input type="hidden" name="kode_alat" value="<?php echo $da->kode_alat ?>">
                <input type="hidden" name="foto_lama" value="<?php echo $da->foto ?>">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Data</h4>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                    <label>KODE BARANG</label>
                    <input type="text" name="nama_alat" class="form-control" required="" value="<?php echo $da->kode_alat; ?>">
                    <label>NAMA BARANG</label>
                    <input type="text" name="nama_alat" class="form-control" required="" value="<?php echo $da->nama_alat ?>">
                    <label>JUMLAH</label>
                    <input type="text" name="jumlah" class="form-control" required="" value="<?php echo $da->kode_alat; ?>">
                    <label>HARGA</label>
                    <input type="text" name="harga" class="form-control" required="" value="<?php echo $da->nama_alat ?>">
                    <label>TGL MASUK</label>
                    <input type="text" name="tgl_masuk" class="form-control" value="<?php echo $da->jumlah ?>">
                    <label>FOTO</label>
                    <input type="file" name="foto" class="form-control" value="<?php echo $da->foto ?>">
                  </div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Update</button>
              </div>
            </div>
            </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal modal-danger fade" id="hapus<?php echo $da->kode_alat ?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hapus Data</h4>
              </div>
              <form action="<?php echo site_url('data_alat/hapus_alat') ?>" method="post">
                <input type="hidden" name="kode_alat" value="<?php echo $da->kode_alat ?>">
              <div class="modal-body">
                <p>Anda Yakin Akan Menghapus Alat ini?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Hapus</button>
              </div>
            </div>
            </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


                  <?php }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      </div>
    </section>

        <div class="modal modal-primary fade" id="tambah">
          <div class="modal-dialog">
            <form action="<?php echo site_url('data_alat/tambah_alat') ?>" method="post" enctype="multipart/form-data" accept-chartset="utf-8">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data</h4>
              </div>
              
              <div class="modal-body">
                  <div class="form-group">
                    <label>Kode Barang</label>
                    <input type="text" name="kode_alat" class="form-control" required>
                    <label>Nama Barang</label>
                    <input type="text" name="nama_alat" class="form-control" required>
                    <label>Jumlah</label>
                    <input type="text" name="jumlah" class="form-control" >
                    <label>Harga</label>
                    <input type="text" name="harga" class="form-control" required>
                    <label>Tgl masuk</label>
                    <input type="date" name="tgl_msk" class="form-control" required>
                    <label>Foto</label>
                    <input type="file" name="foto" class="form-control" >
                  </div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success">Tambah Data</button>
              </div>
            </div>
            </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

   