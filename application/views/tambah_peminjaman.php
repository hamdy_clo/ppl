  
    <section class="content-header">
      <h1>
        Tambah Peminjaman Alat
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Informasi Peminjam</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-4">
                  <label>Nama Penerima : </label>
                  <input class="form-control" type="text" disabled="disabled" name="" value="<?php echo $data_peminjam['nama_penerima'] ?>">
                </div>
                <div class="col-md-4">
                  <label>Nama Penanggung Jawab : </label>
                  <input class="form-control" type="text" disabled="disabled" name="" value="<?php echo $data_peminjam['nama_penanggung_jawab'] ?>">
                </div>
                <div class="col-md-4">
                  <label>Lokasi : </label>
                  <input class="form-control" type="text" disabled="disabled" name="" value="<?php echo $data_peminjam['lokasi'] ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Alat Yang Tersedia</h3>
            </div>


                <div class="box-body table-responsive">
                  <form method="get" action="<?php site_url('data_peminjaman/tambah_pinjaman_alat') ?>">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <td>No</td>
                        <td>Kode Alat</td>
                        <td>Merk</td>
                        <td>Foto</td>
                        <td>Check Box</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1;
                      foreach ($data_alat_tersedia as $dat){ ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $dat->kode_alat ?></td>
                          <td><?php echo $dat->merk ?></td>
                          <td><img class="responsive" src="<?php echo base_url(); ?>assets/gambar/<?php echo $dat->foto ?>" width="200px"></td>
                          <td><input type="checkbox" name="<?php echo $dat->kode_alat ?>" value="N"></td>
                        </tr>
                      <?php } ?>
                      
                    </tbody>
          <!--           <tfoot>
                      <a href="<?php echo site_url('admin/data_peminjaman') ?>"><button class="btn btn-danger pull-left"> Kembali</button></a>
                      
                      <button type="submit" class="btn bnt-info pull-right"> Tambahkan</button>
                    </tfoot> -->
                    
                  </table>
                  <a href="<?php echo site_url('admin/data_peminjaman') ?>">
                    <button class="btn btn-danger pull-left"> Kembali</button>
                  </a>
                  <button type="submit" class="btn btn-info pull-right"> Tambahkan</button>
                  </form>
                </div>
              </div>
          </div>
      </div>
    </section>