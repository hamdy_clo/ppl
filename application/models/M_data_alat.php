<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data_alat extends CI_Model {

	public function tampil_alat(){
		$sql = "SELECT * FROM `data_alat` ORDER BY nama_alat";
		return $this->db->query($sql);
	}
	public function tampil_alat_tersedia(){
		$sql = "SELECT * FROM `data_alat` WHERE tersedia = 'Y' ORDER BY nama_alat";
		return $this->db->query($sql);
	}
	public function tambah_alat($data){
		$this->db->insert('data_alat',$data);
	}
	public function update_alat($kode_alat,$data){
		$this->db->where('kode_alat',$kode_alat);
		$this->db->update('data_alat',$data);
	}
	public function hapus_alat($kode_alat){
		$this->db->where('kode_alat',$kode_alat);
		$this->db->delete('data_alat');
	}
	public function cek_kode_alat($kode_alat){
		$sql = "SELECT * FROM `data_alat` where kode_alat = '".$kode_alat."'";
		return $this->db->query($sql);
	}
}
