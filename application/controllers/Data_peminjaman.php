<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_peminjaman extends CI_Controller {
	public function __construct(){
          parent::__construct();
          $this->load->model('m_data_alat');
          $this->load->model('m_data_peminjaman');
     }
    public function tambah_peminjam(){
    	date_default_timezone_set('Asia/Jakarta');
     		$data = array(
     			'nama_penerima'			=> $this->input->post('nama_penerima'),
     			'nama_penanggung_jawab'	=> $this->input->post('nama_penanggung_jawab'),
     			'lokasi'				=> $this->input->post('lokasi'),
     			'status'				=>'Y',
     			'tanggal_pinjam'		=> date('Y-m-d'));
     		$this->m_data_peminjaman->tambah_peminjam($data);
     		redirect('admin/data_peminjaman?tambah_peminjam#berhasil');
     	}
    public function tambah_peminjam_alat($params){
		// echo $params;
		var_dump($params);
    }
 }