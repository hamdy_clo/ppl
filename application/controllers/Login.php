<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
          parent::__construct();
          $this->load->model('m_login');
     }

     public function index(){
          $this->load->view('login');
     }
     public function login(){
          $data_login = array(
               'username'     => $this->input->post('username'),
               'password'     => md5($this->input->post('password')));
          $cek = $this->m_login->cek_login($data_login)->num_rows();
          if($cek > 0){
               $userdata['status'] = 'online';
               $this->session->set_userdata($userdata);
               redirect('admin');
          }else{
               redirect('');
          }
     }

     public function logout(){
          $this->session->sess_destroy();
          redirect('');
     }
}
	