<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_alat extends CI_Controller {
	public function __construct(){
          parent::__construct();
          $this->load->model('m_data_alat');
     }
     public function tambah_alat(){
     	$kode_alat = $this->input->post('kode_alat');
     	$cek = $this->m_data_alat->cek_kode_alat($kode_alat)->num_rows();
     	if($cek > 0){
     		redirect('admin/data_alat?kode_alat_ada#gagal'); die();
     	}else{
     		$data = array(
     		'kode_alat'	=> $kode_alat,
     		'nama_alat'	=> $this->input->post('nama_alat'),
     		'jumlah'		=> $this->input->post('jumlah'),
               'harga'        => $this->input->post('harga'),
               'tgl_msk'      => $this->input->post('tgl_msk'));
     		$foto		=	$_FILES['foto']['name'];
     		if($foto==''){

     		}else{
     			$config['upload_path']		='assets/gambar';
     			$config['allowed_types']	='jpg|jpeg|png';

     			$this->load->library('upload',$config);

     			if(!$this->upload->do_upload('foto')){
     				echo $this->input->post('foto');
     				echo "gagal upload"; die();
     			}else{
     				$foto	=	$this->upload->data('file_name');
     				$data['foto'] = $foto;
     			}
     		}
     		$this->m_data_alat->tambah_alat($data);
     		redirect('admin/data_alat?tambah_alat#berhasil');
     	}
     	
     }
     public function update_alat(){
     	$kode_alat 	= $this->input->post('kode_alat');
     	$foto_lama	= $this->input->post('foto_lama');
     	$data = array(
     		'nama_alat'	=> $this->input->post('nama_alat'),
     		'jumlah'		=> $this->input->post('merk'));
     	$foto		=	$_FILES['foto']['name'];
     		if($foto==''){

     		}else{
     			$config['upload_path']		='assets/gambar';
     			$config['allowed_types']	='jpg|jpeg|png';

     			$this->load->library('upload',$config);

     			if(!$this->upload->do_upload('foto')){
     				echo $this->input->post('foto');
     				echo "gagal upload"; die();
     			}else{
     				unlink('assets/gambar/'.$foto_lama.'');
     				$foto	=	$this->upload->data('file_name');
     				$data['foto'] = $foto;
     			}
     		}
     	$this->m_data_alat->update_alat($kode_alat,$data);
     	redirect('admin/data_alat?update_alat#berhasil');
     }
     public function hapus_alat(){
     	$kode_alat = $this->input->post('kode_alat');
     	$this->m_data_alat->hapus_alat($kode_alat);
     	redirect('admin/data_alat?hapus_alat#berhasil');
     }
}
	