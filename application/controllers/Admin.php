<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct(){
          parent::__construct();
          $this->load->model('m_data_alat');
          $this->load->model('m_data_peminjaman');
          if($this->session->userdata('status')!='online'){
          	redirect('');
          }
    }
    
    public function index(){
		$this->load->view('header');
		$this->load->view('dashboard');
		$this->load->view('footer');
	}
	
	public function data_alat(){
		$data = array(
			'data_alat'	=> $this->m_data_alat->tampil_alat()->result());
		$this->load->view('header');
		$this->load->view('data_alat',$data);
		$this->load->view('footer');
	}
	
	public function coba(){
		$this->load->view('header');
		$this->load->view('coba');
		$this->load->view('footer');
	}
	
	public function tambah_peminjaman(){
		$id_peminjaman = $this->uri->segment(3);
		$data = array(
			'data_peminjam'			=>	$this->m_data_peminjaman->tampil_peminjam_id($id_peminjaman)->row_array(),
			'data_alat_tersedia'	=>	$this->m_data_alat->tampil_alat_tersedia()->result()); 

		// echo($id_peminjaman);

		$this->load->view('header');
		$this->load->view('tambah_peminjaman',$data);
		$this->load->view('footer');
	}

	public function barang_keluar(){
		$data = array(
			'data_alat'	=> $this->m_data_alat->tampil_alat()->result());
		$this->load->view('header');
		$this->load->view('barang-keluar',$data);
		$this->load->view('footer');
	}
	
	public function stok(){
		$data = array(
			'data_alat'	=> $this->m_data_alat->tampil_alat()->result());
		$this->load->view('header');
		$this->load->view('stok',$data);
		$this->load->view('footer');
	}

}
