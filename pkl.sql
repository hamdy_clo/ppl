-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Des 2018 pada 11.46
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkl`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', '21232F297A57A5A743894A0E4A801FC3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_alat`
--

CREATE TABLE `data_alat` (
  `kode_alat` int(10) NOT NULL,
  `nama_alat` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `foto` varchar(150) NOT NULL,
  `tersedia` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_alat`
--

INSERT INTO `data_alat` (`kode_alat`, `nama_alat`, `merk`, `foto`, `tersedia`) VALUES
(1, 'Amplas Listrik', 'MODERN M-2900', 'sdsds1.jpg', 'Y'),
(2, 'Amplas Listrik', 'NTR PRO 9035 HD', 'maktec_maktec-mt-920-mesin-amplas_full03.jpg', 'Y'),
(3, 'Mesin Circle', 'Maktec MT410', 'Merah', 'Y'),
(4, 'Mesin Circle', 'Mactec MT585', 'Merah', 'Y'),
(5, 'Mesin Profil', 'Mactec MT370', 'Merah', 'Y'),
(6, 'Jigsaw', 'M-2200 L', 'item_XL_5247838_5232262.jpg', 'Y'),
(7, 'Serut Listrik', 'Malkita N 1900 B', '-', 'Y'),
(8, 'Tembakan Silikon', '-', 'Hitam', 'Y'),
(9, 'Tembakan Silikon', '-', 'Putih', 'Y'),
(10, 'Warning Tape ', '-', 'Merahputih', 'Y'),
(11, 'Gergaji', '-', 'maxresdefault1.jpg', 'Y'),
(12, 'Gergaji', '-', 'maxresdefault.jpg', 'Y'),
(13, 'Gergaji', '-', 'maxresdefault2.jpg', 'Y'),
(14, 'Mesin Bor', 'Sander', 'landscape-1446070975-sanders.jpg', 'Y'),
(15, 'Mesin Bor', 'Mactec MT80B', 'sdsds.jpg', 'Y'),
(16, 'Lem tembak', '-', 'index.jpg', 'Y'),
(17, 'Mesin Gerinda', 'Makita 9553B', 'Hijau', 'Y'),
(18, 'Mesin Gerinda', 'Wipro W3436', 'Merah', 'Y'),
(19, 'Paku Tembak', 'NRT Pro F30', 'Merah', 'Y'),
(20, 'Meteran', 'Ats(5m)', 'Kuning', 'Y'),
(21, 'Meteran', 'Roll(Besar)', 'Kuning', 'Y'),
(22, 'Meteran', 'Aldo', 'Kuning', 'Y'),
(23, 'Kunci-kunci', '-', 'images.jpg', 'Y'),
(2762, 'pentil', 'sjd', '', 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_peminjaman`
--

CREATE TABLE `data_peminjaman` (
  `id_peminjaman` int(10) NOT NULL,
  `nama_penerima` varchar(100) NOT NULL,
  `nama_penanggung_jawab` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `status` enum('Y','N') NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_peminjaman`
--

INSERT INTO `data_peminjaman` (`id_peminjaman`, `nama_penerima`, `nama_penanggung_jawab`, `lokasi`, `status`, `tanggal_pinjam`, `tanggal_kembali`) VALUES
(1, 'Hendra', 'Ardian', 'Sahara', 'Y', '2018-11-23', '0000-00-00'),
(2, 'Lalu', 'Didik', 'Sumbawa', 'Y', '2018-11-23', '0000-00-00'),
(3, 'Irwan', 'Didik', 'Sumbawa', 'Y', '2018-11-23', '0000-00-00'),
(4, 'Hendra', 'Didik', 'Sumbawa', 'Y', '2018-11-24', '0000-00-00'),
(5, 'Ardian', 'putra', 'Sahara', 'Y', '2018-11-24', '0000-00-00'),
(6, 'ardian', 'putra', 'dwdw', 'Y', '2018-11-24', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pinjam_alat`
--

CREATE TABLE `data_pinjam_alat` (
  `id_peminjaman` int(10) NOT NULL,
  `kode_alat` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `data_alat`
--
ALTER TABLE `data_alat`
  ADD PRIMARY KEY (`kode_alat`);

--
-- Indeks untuk tabel `data_peminjaman`
--
ALTER TABLE `data_peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_peminjaman`
--
ALTER TABLE `data_peminjaman`
  MODIFY `id_peminjaman` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
